<?php
require('connect.php');

$currency = '&#8377; '; //Currency Character or code

$db_username = 'root';
$db_password = '12345678';
$db_name = '018';
$db_host = 'localhost';

$shipping_cost      = 1.50; //shipping cost
$taxes              = array( //List your Taxes percent here.
                            'VAT' => 12, 
                            'Service Tax' => 5
                            );						
//connect to MySql						
$mysqli = new mysqli($db_host, $db_username, $db_password,$db_name);						
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
session_start();
?>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
 <title>D-NOODlE</title>
 </head>
 <body>
 <?php include 'nav.php';?>
 <?php
	if(isset($_SESSION["cart_products"]) && count($_SESSION["cart_products"])>0)
	{
		$total =0;
		$b = 0;
		foreach ($_SESSION["cart_products"] as $cart_itm)
		{
			$product_name = $cart_itm["product_name"];
			$product_qty = $cart_itm["product_qty"];
			$product_price = $cart_itm["product_price"];
			$product_code = $cart_itm["product_code"];
			$product_color = $cart_itm["product_color"];
			$bg_color = ($b++%2==1) ? 'odd' : 'even'; //zebra stripe
			$subtotal = ($product_price * $product_qty);
			$total = ($total + $subtotal);
			$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
			
		}
	}
 ?>
<!--========เนื้อหา===========-->
<table style="margin-top:20px;">
<tr align="center">
<td>
<?php
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products ORDER BY id ASC");
if($results){ 
$products_item = '<ul class="products">';
//fetch results set as object and output HTML
while($obj = $results->fetch_object())
{
$products_item .= <<<EOT
	<li class="product" style="display:inline-block;">
	<form method="post" action="cart_update1.php">
	<div class="card-group" style="width:20rem;">
	<div class="card" style="background:#212530;border-radius:8px;">
	<label align="center">
    <img class="card-img-top" src="pictures/{$obj->product_img_name}" height="250" alt="Card image cap" style="border-radius:6px;">
	<label>
    <div class="card-body">      
     <span style="color:#FFF">Quantity</span>
		<input  style="height:15;width:42" type="text" size="2" maxlength="2" name="product_qty" value="1" />	
		<input type="hidden" name="product_code" value="{$obj->product_code}" />
		<input type="hidden" name="type" value="add" />
		<input type="hidden" name="return_url" value="{$current_url}" />
		<button class="button" type="submit" class="add_to_cart" name="add_to_cart"style="vertical-align:middle;width:90;height:35"><span>{$obj->price}</span></button>
    </div>
  </div>  
</div>
	
	</form>
	</li>
EOT;
}
$products_item .= '</ul>';
echo $products_item;

}
?>    
</td>
</tr>
</table>				
<!--=====================สิ้นสุด==================-->
</body>
</html>

<style>

html,body
 {
 height:100%;
 margin:0px;
 }
 body{
 background-color:#392b29;
 background-image: url("pictures/BG03.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;
 }

.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 16px;
  padding: 10px 20px 10px 20px;
  width: 100px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: 'BUY';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -30px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 40px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
</style>