<?php
require('connect.php'); 
session_start();
?>
<html lang="en">
 <head>
  <meta charset="UTF-8">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <title>D-Noodle</title>
 </head>
 <body>

 <?php
$id = $_SESSION['cid'];

$sexsql = "SELECT * FROM sex";
$sexresult = mysqli_query($con,$sexsql);
$sex = mysqli_fetch_all($sexresult,MYSQLI_ASSOC);

$provincesql = "SELECT * FROM provinces";
$provinceresult = mysqli_query($con,$provincesql);
$provinces = mysqli_fetch_all($provinceresult,MYSQLI_ASSOC);


$sql = "SELECT * FROM customers WHERE cid=$id";
$result = mysqli_query($con,$sql);
$oldcustomer = mysqli_fetch_all($result,MYSQLI_ASSOC);

if(isset($_POST['edit'])) {

  $fullname = $_POST['fullname'];
  $username = $_POST['username'];
  $email = $_POST['email'];  
  $province = $_POST['province'];
 $tel=$_POST['tel'];
 $at=$_POST['at'];
 $sex=$_POST['sex'];

  $sql = "UPDATE customers SET fullname='$fullname' , username='$username' ,  email='$email' , province='$province'  , tel='$tel' , at='$at' , sex='$sex' WHERE cid=$id";
  mysqli_query($con,$sql);
  $_SESSION['fullname'] =  $fullname;
  header('location:index.php');

}

?>


<table id ="main_layout" cellpadding="0" cellspacing="0">
<!--menuL lockPosition-->
<tr>
	<td height="100%" width="31%">
	</td>
<!--N menuL lockPosition-->

<!--Right spac -->
	<td height="100%" width="69%">	

	<table height="100%" width="100%" >
	<tr>
		<td height="100px" width="100%" valign="top" >
		<div class="Menu">	
			<ul>
				<li><font size="+2"><a href="index.php"><i class="fa fa-home fa-fw" aria-hidden="true"></i>HOME</a></li>
				<li><a href="15.php"><i class="fa fa-list fa-fw" aria-hidden="true"></i>Menu</a></li>
				<?php
				if (isset($_SESSION['cid'])){
				echo'<div id="Cart"><li><a href="11.php"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i>Cart</a></li></div>';
				echo'<div id="Bill"><li><a href="9.php"><i class="fa fa-list-alt fa-fw" aria-hidden="true"></i>Order</a></li></div>';		
				echo '<div id="Register"><li><a href="6.php"><i class="fa fa-user fa-fw" aria-hidden="true"></i>'.$_SESSION['username'].'</a></li></div>';
				echo'<div id="login"><li><a href="3.php" ><i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>Logout</a></font></li></div>';				
				}else{
				echo'<div id="Cart"><li><a href="11-1.php"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i>Cart</a></li></div>';
				echo'<div id="Bill"><li><a href="9-1.php"><i class="fa fa-list-alt fa-fw" aria-hidden="true"></i>Order</a></li></div>';		
				echo '<div id="Register"><li><a href="5.php"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Register</a></li></div>';
				echo'<div id="login"><li><a href="3.php" ><i class="fa fa-sign-in fa-fw" aria-hidden="true"></i>Login</a></font></li></div>';
				}
				?>			
<div id="class" valign="right"><li><marquee scrollamount="3" direction="down"><img src="pictures/G1.gif" width="90" style="opacity:0.5;"></marquee></li></div>				
			</ul>
		</div>
		</td>
	</tr>
	<tr>
		<td width="100%">
		
		<!--=====================เริ่มต้น==================-->		
		
				<table height="100%" width="100%" style="color:#fff" >
				<tr>
					<td> 
					<form align="center" name="edit" method="post" action="">
					<h1>Edit Profile</h1>
						<table height="70%" width="90%" style="color:#fff">
						<tr>
							<td width="30%">Full Name :</td>
							<td width="70%"><input type="text" class="form-control" name="fullname" value="<?php echo $oldcustomer[0]['fullname']; ?>"></td>
						</tr>
						<tr>
							<td width="30%">Username :</td>
							<td width="70%"><input type="text" class="form-control" name="username" value="<?php echo $oldcustomer[0]['username']; ?>"></td>
						</tr>
						<tr>
							<td width="30%">E-mail :</td>
							<td width="70%"><input type="email" class="form-control" name="email" value="<?php echo $oldcustomer[0]['email']; ?>"></td>
						</tr>
							<tr>
							<td width="30%">Tel :</td>
							<td width="70%"><input type="text" class="form-control" name="tel" value="<?php echo $oldcustomer[0]['tel']; ?>"></td>
						</tr>
							<tr>
							<td width="30%">Address :</td>
							<td width="70%"><input type="text" class="form-control" name="at" value="<?php echo $oldcustomer[0]['at']; ?>"></td>
						</tr>
						<tr>
							<td width="30%">Sex :</td>
							<td width="70%"><br>
							<select class="form-control" name="sex" style="border-color:#be8943;background:#222631;color:#fff;padding:2px 5px 2px 5px;">
							  <?php
							  foreach ($sex as $sex) {
									if ($oldcustomer[0]['sex'] == $sex['sid']) {
									echo '<option value="' . $sex['sid'] . '" selected>' . $sex['sname'] . "</option>";
								  } else {
									echo '<option value="' . $sex['sid'] . '">' . $sex['sname'] . "</option>";
								  }
							  }
							  ?>
							  </select>
						<tr>
							<td width="30%">Province :</td>
							<td width="70%"><br>
							<select class="form-control" name="province" style="border-color:#be8943;background:#222631;color:#fff;padding:2px 5px 2px 5px;">
							  <?php
							  foreach ($provinces as $province) {
									if ($oldcustomer[0]['province'] == $province['pid']) {
									echo '<option value="' . $province['pid'] . '" selected>' . $province['pname'] . "</option>";
								  } else {
									echo '<option value="' . $province['pid'] . '">' . $province['pname'] . "</option>";
								  }
							  }
							  ?>
							  </select>							  
							  <br>
							</td>
						</tr>						
						</table>
						<br>
						<br>						
						<button  type="submit" name="edit" style="color:#bdc3c7; text-decoration:none;">Submit</button>				  
				</form>
				</td>
				</tr>
				</table>
			<!--=====================สิ้นสุด==================-->
		
		</td>
	</tr>
	<tr>
		<td height="64px" width="100%" valign="top">	
		<a href="36.php"><img src="pictures/contact.gif" height="43" align="right" valign="buttom" style="padding:0px 5px 0px 10px"></a>
		<a href="https://www.facebook.com/tamooony.10"><img src="pictures/facebook.gif" height="43" align="right" valign="buttom"></a>
		</td>
	</tr>
	</table>

	
	</td>
</tr>
<!--NRight spac -->
</table>
 </body>
</html>
