<?php
require('connect.php'); 
session_start();
?>

<?php
$id = $_SESSION['cid'];

$sexsql = "SELECT * FROM sex";
$sexresult = mysqli_query($con,$sexsql);
$sex = mysqli_fetch_all($sexresult,MYSQLI_ASSOC);

$provincesql = "SELECT * FROM provinces";
$provinceresult = mysqli_query($con,$provincesql);
$provinces = mysqli_fetch_all($provinceresult,MYSQLI_ASSOC);


$sql = "SELECT * FROM customers WHERE cid=$id";
$result = mysqli_query($con,$sql);
$oldcustomer = mysqli_fetch_all($result,MYSQLI_ASSOC);

if(isset($_POST['edit'])) {

  $fullname = $_POST['fullname'];
  $username = $_POST['username'];
  $email = $_POST['email'];  
  $province = $_POST['province'];
 $tel=$_POST['tel'];
 $at=$_POST['at'];
 $sex=$_POST['sex'];
  $pic=$_POST['pic'];


  $sql = "UPDATE customers SET fullname='$fullname' , username='$username' ,  email='$email' , province='$province'  , tel='$tel' , at='$at' , sex='$sex' ,pic='$pic'  WHERE cid=$id";
  mysqli_query($con,$sql);
  $_SESSION['fullname'] =  $fullname;
  header('location:index.php');

}

?>

<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
$('[name="pic"]').change(function(){
 
    /* original file upload path */
    $('#picPath').text($(this).val());
 
    var file = this.files[0];
    /* get picture details */
    $('#picLastModified').text(file.lastModified);
    $('#picLastModifiedDate').text(file.lastModifiedDate);
    $('#picName').text(file.name);
    $('#picSize').text(file.size);
    $('#picType').text(file.type);
 
    /* set image preview */
    if( ! file.type.match(/image.*/))
    {
        return true;
    }
    var reader = new FileReader();
    reader.onload = function (event)
    {
        $('#picPreview').attr('src', event.target.result);
 
        /* get image dimensions */
        var image = new Image();
        image.src = reader.result;
        image.onload = function()
        {
            $('#picDimensionsWidth').text(image.width);
            $('#picDimensionsHeight').text(image.height);
        };
 
    }
    reader.readAsDataURL(file);
 
});



</script>



<html lang="en">
<meta charset="UTF-8">
<head>
  <title>D-NOODlE</title>
 </head>
 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<body>
<?php include 'nav.php';?>
<!--=====================เริ่มต้น==================-->	
<div class="container" align="center" style="border-style:solid;border-width:1px;border-radius:5px;border-color:#624620;margin-top:10px;background: rgba(0, 0, 0, 0.2);">
	<div class="row">
		<div class="col">		
		<form id="demo1" name="register" method="post" action=""  onSubmit="JavaScript:return fncSubmit();" >				
				<h1 align="center">Edit</h1>
				<div class="container">
					<br>
					<div class="row">
						<div class="col-sm" >	
							<form action="" method="post" enctype="multipart/form-data">
								 <label>
								 <?php echo "<img src='picmem/" . $oldcustomer[0]["pic"] ." ' width='220'>"; ?>
								 <input type="file" name="pic" accept="imag/*">
								 </label>
							</form>
						</div>
					</div>
					<div class="row">						
						<div class="col">
						<table width="100%">
						<tr>
						<td width="100px" style="color:#fff;">Username :</td>
						<td>
						<input type="text" name="username" placeholder="Username" disabled="disabled" value="<?php echo $oldcustomer[0]['username']; ?>"> 
						</td>
						</tr>
						</table>						
						</div>
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Password :</td>
						<td>
						<input type="text" name="password" placeholder="Password"  value="<?php echo $oldcustomer[0]['password']; ?>">
						</td>
						</tr>
						</table>
						</div>
	
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Name :</td>
						<td>
						<input type="text" name="fullname" placeholder="Name"  value="<?php echo $oldcustomer[0]['fullname']; ?>">
						</td>
						</tr>
						</table>
						</div>						
					</div>
					<div class="row" style="margin-top:5px;">
						<div class="col-sm"> 
										<table width="100%">
										<tr>
										<td  width="100px" style="color:#fff;">Gender :</td>
										<td>
										<select name="sex" class="form-control" style="border-color:#be8943;background:#222631;color:#fff;" >
										<option value="เพศ" >Gender
										 <?php
												$sql="select * from sex";
												$result=mysqli_query($con,$sql);
												while($row2=mysqli_fetch_assoc($result))
												{
													$selected=($row[sid] == $row[sid])?:"";
										echo "<option value='.$row2[sid].' $selected selected>$row2[sname]";
												}
										?>
										</select>		
										</td>
										</tr>
										</table>												
						</div>
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Email :</td>
						<td>
						<input type="email" name="email" placeholder="Email"  value="<?php echo $oldcustomer[0]['email']; ?>">
						</td>
						</tr>
						</table>
						</div>						
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Tel :</td>
						<td>
						<input type="text" name="tel" placeholder="Tel"  value="<?php echo $oldcustomer[0]['tel']; ?>">
						</td>
						</tr>
						</table>
						</div>	
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Address :</td>
						<td>
						<input type="text" name="at" placeholder="Address"  value="<?php echo $oldcustomer[0]['at']; ?>">
						</td>
						</tr>
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">District :</td>
						<td>
						<div class="uk-width-1-2@m">
						<input type="text" name="district" placeholder="District" class="uk-input uk-width-1-1" style="background:transparent;" value="<?php echo $oldcustomer[0]['district']; ?>">
						</div>
						</td>
						<td  width="100px" style="color:#fff;">Amphoe :</td>
						<td>
						<div class="uk-width-1-2@m">
				  		<input type="text" name="amphoe" placeholder="Amphoe" class="uk-input uk-width-1-1" style="background:transparent;" value="<?php echo $oldcustomer[0]['amphoe']; ?>">
						</div>
						</td>
						</tr>
						</table>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm">
						<table width="100%">
						<tr>
						<td  width="100px" style="color:#fff;">Province :</td>
						<td>
						<div class="uk-width-1-2@m">
				  		<input type="text" name="province" placeholder="Province" class="uk-input uk-width-1-1" style="background:transparent;" value="<?php echo $oldcustomer[0]['province']; ?>">
						</div>
						</td>
						<td  width="100px" style="color:#fff;">Zipcode :</td>
						<td>
						<div class="uk-width-1-2@m">
						<input type="text" name="zipcode" placeholder="Zipcode" class="uk-input uk-width-1-1" style="background:transparent;" value="<?php echo $oldcustomer[0]['zipcode']; ?>">
						</div>
						</td>
						</tr>
						</table>
						</div>
					</div>
					<br>
					<div class="row">	  
					<div class="col-sm">
						  <button type="submit" name="edit" 
						  style="background:transparent;width:30%;font-size:1.3em;border:solid 1px #be8943;padding: 0.5em ;color:#bdc3c7;transition:all 0.6s;border-radius:5px"> Submit</button>
					</div>
				</div>
			</div> <!--===ปิดคอนเทรนเนอร์2===-->
			</form>
		</div>
	</div>
</div>							
			<!--=====================สิ้นสุด==================-->
			
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
$('[name="pic"]').change(function(){
 
    /* original file upload path */
    $('#picPath').text($(this).val());
 
    var file = this.files[0];
    /* get picture details */
    $('#picLastModified').text(file.lastModified);
    $('#picLastModifiedDate').text(file.lastModifiedDate);
    $('#picName').text(file.name);
    $('#picSize').text(file.size);
    $('#picType').text(file.type);
 
    /* set image preview */
    if( ! file.type.match(/image.*/))
    {
        return true;
    }
    var reader = new FileReader();
    reader.onload = function (event)
    {
        $('#picPreview').attr('src', event.target.result);
 
        /* get image dimensions */
        var image = new Image();
        image.src = reader.result;
        image.onload = function()
        {
            $('#picDimensionsWidth').text(image.width);
            $('#picDimensionsHeight').text(image.height);
        };
 
    }
    reader.readAsDataURL(file);
 
});
</script>			
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
    
    <!-- dependencies for zip mode -->
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
    <!-- / dependencies for zip mode -->

    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    
    <script type="text/javascript" src="./jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
    
    <script type="text/javascript">
        /******************\
         *     DEMO 1     *
        \******************/ 
        // demo 1: load database from json. if your server is support gzip. we recommended to use this rather than zip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

        /******************\
         *     DEMO 2     *
        \******************/ 
        // demo 2: load database from zip. for those who doesn't have server that supported gzip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.zip', 
            $search: $('#demo2 [name="search"]'),

            onDataFill: function(data){
                console.log(data)
                var html = '<b>ที่อยู่:</b> ตำบล' + data.district + ' อำเภอ' + data.amphoe + ' จังหวัด' + data.province + ' ' + data.zipcode;
                $('#demo2-output').prepend('<div class="uk-alert-warning" uk-alert><a class="uk-alert-close" uk-close></a>' + html + '</div>');
            }

        });
    </script>

</body>
</html>

<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#3b2d2a;
 background-image: url("pictures/BG03.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;

}

#main_layout{
	width:100%;
	height:100%;	
}

{
@import url(http://fonts.googleapis.com/css?family=Roboto:400,100);
}

#Register li a {
	border-style: solid;
    border-width: 1px 1px 1px 1px;
	border-color: #587c88;
	box-shadow:0px 0px 5px #ffffff;	
}

#Register li a:hover:not(.active) {    
	color:#be8943;
	border-style: solid;
    border-width: 1px 1px 1px 1px;
	border-color:#be8943;
	box-shadow:0px 0px 1px #be8943;
	}
#nameRegister{
 text-shadow: 0px 2px 3px #ffcc33;
 }

form {
  position:center;
  width: 100%;
 
  
}
input {
  width: 100%;
  background: transparent;
  border-bottom: solid 1px #be8943;
  border-top: none;
  border-left: none;
  border-right: none;
  font-size: 1.3em;
  padding: 0.5em 0.4em;
  transition: all 0.4s;
  color: #ffffff;
}
input:focus {  
  transform: scale3d(1.06,1.06,1.06);
  
}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  width: 40%;
}
@media (max-width: 550px) {
  form {
  width: 100%;
  padding-top: 5%;
}
  input {
    font-size: 1em;
  }
}

select{
border-color:#be8943;
background: transparent;
color:#fff;
border-style: solid;
border-width: 1px 1px 1px 1px; 
}

</style>