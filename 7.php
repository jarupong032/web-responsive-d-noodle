<?php
require('connect.php'); 
session_start();
?>
<html lang="en">
 <head>
  <title>D-NOODlE</title>
 </head>
 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<body>
<?php include 'nav.php';?>		
<!--=====================เริ่มต้น==================-->	
<div class="container" align="center" style="margin-top:300px">
	<div class="row">				
		<div class="col-sm">							  
				<form align="center">
				  <h1 style="color:#fff;">Thank you for regis</h1>				 
				  <INPUT TYPE="BUTTON" class="btn btn-warning" VALUE="Success" ONCLICK="window.location.href='3.php'" style="width:250px">
				</form>			
		</div>
	</div>
</div>
<!--=====================สิ้นสุด==================-->		
</body>
</html>

<style>

html,body
 {
 height:100%;
 margin:0px;
 }
 body{
 background-color:#392b29;
 background-image: url("pictures/BG03.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;
 }
 
.btn-warning{
color:#fff;
background:#21242f;
border-color:#be8943;
background: transparent;
	}
 .btn-warning:hover {     
 color:#be8943;
 background:#382a28;
	}


 hr {
    height: 1px;
    color: #be8943;
    background: #be8943;
	}


#login li a {
	border-style: solid;
    border-width: 1px 1px 1px 1px;
	border-color: #be8943;
	box-shadow:0px 0px 10px #587c88;
	
}

#login li a:hover:not(.active) {    
	color:#be8943;
	border-style: solid;
    border-width: 1px 1px 1px 1px;
	border-color:#be8943;
	box-shadow:0px 0px 10px #be8943;
	}


#Forgot{
width:50px;
color:#ffffff;
background-color:#222631;
border-style: groove;
border-color: #be8943;
}


#Forgot:hover{
 cursor: pointer;
 color:#ffffff;
 text-shadow:0px 0px 5px #ffff00; 
 box-shadow:0px 0px 5px #ffff00;
}


#login1{
text-shadow:0px 0px 2px #000000;
}


{
@import url(http://fonts.googleapis.com/css?family=Roboto:400,100);
}

.login-card {
  padding: 40px;
  width: 300px;
  background-color:#222631 ;
  margin: 0 auto 10px;
  border-radius: 2px; 
  border-style: groove;
  border-color: #be8943;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  overflow: hidden;
  transition: all 0.4s;
}

.login-card h1 {
  font-weight: 100;
  text-align: center;
  font-size: 2.3em;
}

.login-card input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  position: relative;
}

.login-card input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  -webkit-appearance: none;
  color:#be8943;
  background: #222631;
  border: 1px solid #587c88;
  border-top: 1px solid #587c88;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.login-card input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

.login {
  text-align: center;
  font-size: 14px;
  font-family: 'Arial', sans-serif;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
/* border-radius: 3px; */
/* -webkit-user-select: none;
  user-select: none; */
}

.login-submit {
  /* border: 1px solid #3079ed; */
  border: 0px;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1); 
  background-color: #4d90fe;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.login-submit:hover {
  /* border: 1px solid #2f5bb7; */
  border: 0px;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.login-card a {
  text-decoration: none;
  color: #ffffff;
  font-weight: 400;
  text-align: center;
  display: inline-block;
  opacity: 0.6;
  transition: opacity ease 0.5s;
}

.login-card a:hover {
  opacity: 1;
}

.login-help {
  width: 100%;  
  text-align: center;
  font-size: 12px;
}



</style>