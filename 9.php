<?php
require('connect.php'); 
session_start();
?>
<html lang="en">
<head>
  <title>D-NOODlE</title>
 </head>
 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<body>
<?php include 'nav.php';?>
<table id ="main_layout" cellpadding="0" cellspacing="0" width="100%" align="center">
	<tr>
		<td>		
		<!--=====================เริ่มต้น==================-->
				<table height="100%" width="100%">
				<tr>
					<td style="padding:20px 0px 5px 5%">					
					<button style="color:#FFF;background:#dc4b1d;border-radius:5px;border:2px solid #f4511e;padding: 4px 12px;" type="submit"
					onclick="window.location.href='9-2.php';">Ordered</button>
					</td>
				</tr>
				<tr>
					<td valign="top" align="center">
					<form method="post" action="cart_update.php">
	<table width="90%"  cellpadding="3" border="1" cellspacing="0" style="margin:0 0 0 0;color:#fff;border:1px solid #be8943">
	<thead>
	<tr>
		<th style="width:70">Quantity</th>
		<th>Name</th>
		<th>Price</th>
		<th>Total</th>
	</tr>
	</thead>
	<tbody>
 	<?php
	if(isset($_SESSION["cart_products"])) //check session var
    {
		$total = 0; //set initial total value
		$b = 0; //var for zebra stripe table 
		foreach ($_SESSION["cart_products"] as $cart_itm)
        {
			//set variables to use in content below
			$product_name = $cart_itm["product_name"];
			$product_qty = $cart_itm["product_qty"];
			$product_price = $cart_itm["product_price"];
			$product_code = $cart_itm["product_code"];
			$product_color = $cart_itm["product_color"];
			$subtotal = ($product_price * $product_qty); //calculate Price x Qty
			
		   	$bg_color = ($b++%2==1) ? 'odd' : 'even'; //class for zebra stripe 
		    echo '<tr class="'.$bg_color.'">';
			echo '<td align="center">'.$product_qty.'</td>';
			echo '<td>'.$product_name.'</td>';
			echo '<td align="center">'.$currency.$product_price.'</td>';
			echo '<td align="center">'.$currency.$subtotal.'</td>';
            echo '</tr>';
			$total = ($total + $subtotal); //add subtotal to total var
        }
		
		$grand_total = $total + $shipping_cost; //grand total including shipping cost
		foreach($taxes as $key => $value){ //list and calculate all taxes in array
				$tax_amount     = round($total * ($value / 100));
				$tax_item[$key] = $tax_amount;
				$grand_total    = $grand_total + $tax_amount;  //add tax val to grand total
		}
		$list_tax       = '';
		foreach($tax_item as $key => $value){ //List all taxes
			$list_tax .= $key. ' : '. $currency. sprintf("%01.2f", $value).'<br />';
		}
		$shipping_cost = ($shipping_cost)?'Shipping Cost : '.$currency. sprintf("%01.2f", $shipping_cost).'<br />':'';		
	}
    ?>
    <tr>
		<td colspan="5"><span style="float:right;text-align: right;">
			<?php echo $shipping_cost. $list_tax; ?>Amount Payable : <?php echo sprintf("%01.2f", $grand_total);?> B.</span>
		</td>
	</tr>    
  </tbody>
</table>
<input type="hidden" name="return_url" value="<?php 
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
echo $current_url; ?>" />
</form>
<form align="center" style="margin:0 0 0 0;">
<input type="button" onclick="window.location.href='printproduct.php';" name="" style="height:35;width:120;border-width:1;font-size:18px;" value="Print">
</form>
				</td>
				</tr>
				</table>
			<!--=====================สิ้นสุด==================-->		
</table>
 </body>
</html>

<style>

html,body
 {
 height:100%;
 margin:0px;
 }
 body{
 background-color:#392b29;
 background-image: url("pictures/BG03.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;
 }
 hr {
    height: 1px;
    color: #be8943;
    background: #be8943;
	}
{
@import url(http://fonts.googleapis.com/css?family=Roboto:400,100);
}
input {
  width: 25%;
  background: transparent;
  border:solid 1px #be8943;  
  border-radius:5px;
  font-size: 1.3em;  
  transition: all 0.4s;  
  color: #bdc3c7;
}

input:hover {  
  background: transparent;
  border:solid 1px #587c88;  
  font-size: 1.3em; 
  transition: all 0.4s;  
  color: #bdc3c7;
  box-shadow:0px 0px 14px #ff9900;
}

</style>