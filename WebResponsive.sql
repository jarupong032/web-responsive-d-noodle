-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2017 at 04:50 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `018`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `bid` int(11) NOT NULL,
  `cname` varchar(20) NOT NULL,
  `date` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bid`, `cname`, `date`) VALUES
(3, 'suthathip', '03:05:2017'),
(4, 'suthathip', '04:05:2017');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `cid` int(5) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(80) NOT NULL,
  `status` int(1) NOT NULL,
  `tel` int(20) NOT NULL,
  `sex` int(11) NOT NULL,
  `at` varchar(80) NOT NULL,
  `district` varchar(255) NOT NULL,
  `amphoe` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `pic` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cid`, `fullname`, `username`, `password`, `email`, `status`, `tel`, `sex`, `at`, `district`, `amphoe`, `province`, `zipcode`, `pic`) VALUES
(1, 'wongsatorn ', 'mavelus', '123456', 'mavelus@jk.com', 2, 12355568, 0, '45/58', 'หันคา', 'หันคา', 'ชัยนาท', 17130, 'mvl1.jpg'),
(107, 'jarupong', 'macza', '654321', 'maczazaza@hotmail.com', 2, 14752369, 0, '29/5', 'นางัว', 'บ้านแพง', 'นครพนม', 48140, 'a3.jpg'),
(108, 'suthathip', 'ploy', '123456', 'aaaa@dfgfgdf.com', 1, 1234587, 2, '25/45', 'กรุงชิง', 'นบพิตำ', 'นครพนม', 80160, 'mvl1.jpg'),
(109, 'niwat', 'got', '123456', 'aaaa@dfgfgdf.com', 1, 8542103, 0, '25/78', 'ปากน้ำ', 'เมืองระนอง', 'ระนอง', 85000, 'sibe010.jpg'),
(115, 'ddd', 'sdsad', '123456', 'dsadas@dads.com', 1, 123456, 0, 'asd', 'นางัว', 'บ้านแพง', 'นครพนม', 48140, '531.jpg'),
(118, 'suthathip', 'ployy', '1234', 'ploy@hotmail.com', 1, 9896765, 0, 'จรัญ 42', 'คลองสองต้นนุ่น', 'ลาดกระบัง', 'กรุงเทพมหานคร', 10520, '323.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrdersID` int(11) NOT NULL,
  `OrderDate` varchar(50) NOT NULL,
  `UserID` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrdersID`, `OrderDate`, `UserID`) VALUES
(49, '2017-05-10 09:50:40', 82),
(50, '2017-05-10 09:50:46', 82),
(53, '2017-05-10 14:39:19', 82),
(54, '2017-05-10 14:48:03', 82),
(55, '2017-05-10 14:48:11', 82),
(58, '2017-05-10 15:14:56', 87),
(59, '2017-05-10 15:45:01', 82),
(63, '2017-05-10 15:50:02', 82),
(64, '2017-05-16 06:34:05', 0),
(65, '2017-05-16 06:35:04', 0),
(66, '2017-05-16 06:36:05', 88),
(67, '2017-05-16 06:37:00', 88),
(68, '2017-05-16 06:53:18', 0),
(69, '2017-05-16 07:26:51', 88),
(70, '2017-05-16 10:58:24', 94),
(71, '2017-05-17 18:16:51', 106),
(72, '2017-05-18 05:03:39', 111),
(73, '2017-05-18 06:18:59', 112),
(74, '2017-05-23 16:39:43', 109);

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `DetailID` int(11) NOT NULL,
  `OrdersID` int(50) NOT NULL,
  `Productcode` varchar(50) NOT NULL,
  `Qty` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`DetailID`, `OrdersID`, `Productcode`, `Qty`) VALUES
(10, 49, 'PD1001', 1),
(11, 49, 'PD1002', 1),
(12, 49, 'PD1003', 1),
(13, 50, 'PD1001', 1),
(14, 50, 'PD1002', 1),
(15, 50, 'PD1003', 1),
(16, 50, 'PD1004', 1),
(17, 51, 'PD1001', 1),
(18, 51, 'PD1002', 1),
(19, 51, 'PD1003', 1),
(20, 51, 'PD1004', 1),
(21, 52, 'PD1001', 1),
(22, 52, 'PD1002', 1),
(23, 52, 'PD1003', 1),
(24, 52, 'PD1004', 1),
(25, 53, 'PD1001', 1),
(26, 53, 'PD1002', 1),
(27, 53, 'PD1003', 1),
(28, 53, 'PD1004', 1),
(29, 54, 'PD1001', 1),
(30, 54, 'PD1002', 1),
(31, 54, 'PD1003', 1),
(32, 54, 'PD1004', 1),
(33, 55, 'PD1001', 1),
(34, 55, 'PD1002', 1),
(35, 55, 'PD1003', 1),
(36, 55, 'PD1004', 1),
(37, 56, 'PD1001', 1),
(38, 56, 'PD1002', 1),
(39, 56, 'PD1003', 1),
(40, 56, 'PD1004', 1),
(41, 57, 'PD1001', 1),
(42, 57, 'PD1002', 1),
(43, 57, 'PD1003', 1),
(44, 57, 'PD1004', 1),
(45, 58, 'PD1001', 1),
(46, 58, 'PD1002', 5),
(47, 58, 'PD1003', 1),
(48, 59, 'PD1002', 1),
(49, 59, 'PD1003', 1),
(50, 60, 'PD1002', 1),
(51, 60, 'PD1003', 1),
(52, 61, 'PD1001', 1),
(53, 61, 'PD1002', 1),
(54, 61, 'PD1003', 1),
(55, 61, 'PD1004', 1),
(56, 61, 'PD1008', 1),
(57, 61, 'PD1007', 1),
(58, 61, 'PD1006', 1),
(59, 61, 'PD1005', 1),
(60, 61, 'PD1009', 1),
(61, 61, 'PD1010', 1),
(62, 61, 'PD1011', 1),
(63, 62, 'PD1001', 1),
(64, 62, 'PD1002', 1),
(65, 62, 'PD1003', 1),
(66, 62, 'PD1004', 1),
(67, 62, 'PD1008', 1),
(68, 62, 'PD1007', 1),
(69, 62, 'PD1006', 1),
(70, 62, 'PD1005', 1),
(71, 63, 'PD1001', 1),
(72, 63, 'PD1002', 1),
(73, 63, 'PD1003', 1),
(74, 63, 'PD1004', 1),
(75, 63, 'PD1008', 1),
(76, 63, 'PD1007', 1),
(77, 63, 'PD1006', 1),
(78, 63, 'PD1005', 1),
(79, 65, 'PD1001', 1),
(80, 65, 'PD1002', 1),
(81, 65, 'PD1003', 1),
(82, 65, 'PD1007', 1),
(83, 65, 'PD1006', 1),
(84, 66, 'PD1001', 1),
(85, 66, 'PD1002', 1),
(86, 66, 'PD1003', 1),
(87, 66, 'PD1004', 1),
(88, 66, 'PD1007', 1),
(89, 66, 'PD1008', 1),
(90, 66, 'PD1006', 1),
(91, 67, 'PD1001', 1),
(92, 67, 'PD1002', 1),
(93, 67, 'PD1003', 1),
(94, 67, 'PD1004', 1),
(95, 67, 'PD1008', 1),
(96, 67, 'PD1007', 1),
(97, 67, 'PD1006', 1),
(98, 67, 'PD1005', 1),
(99, 68, 'PD1002', 1),
(100, 69, 'PD1002', 1),
(101, 70, 'PD1002', 1),
(102, 70, 'PD1001', 1),
(103, 70, 'PD1005', 1),
(104, 71, 'PD1001', 1),
(105, 71, 'PD1002', 1),
(106, 71, 'PD1003', 1),
(107, 71, 'PD1007', 1),
(108, 72, 'PD1001', 5),
(109, 72, 'PD1006', 3),
(110, 72, 'PD1007', 3),
(111, 73, 'PD1002', 1),
(112, 73, 'PD1003', 1),
(113, 74, 'PD1001', 1),
(114, 74, 'PD1002', 1),
(115, 74, 'PD1003', 1),
(116, 74, 'PD1004', 1),
(117, 74, 'PD1008', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_code` varchar(60) NOT NULL,
  `product_name` varchar(60) NOT NULL,
  `product_desc` tinytext NOT NULL,
  `product_img_name` varchar(60) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_code`, `product_name`, `product_desc`, `product_img_name`, `price`) VALUES
(1, 'PD1001', 'เย็นตาโฟเครื่อง', '', '41.jpg', '50'),
(2, 'PD1002', 'เกี๊ยวหมูแดง', '', '42.jpg', '50'),
(3, 'PD1003', 'ต้มยำหมูสับ', '', '43.jpg', '40'),
(4, 'PD1004', 'น้ำใสลูกชิ้นปลา', '', '44.jpg', '40'),
(5, 'PD1005', 'ต้มยำกุ้งไข่ยางมะตูม', '', '45.jpg', '45'),
(6, 'PD1006', 'ต้มยำน้ำข้น', '', '46.jpg', '50'),
(7, 'PD1007', 'ต้มยำน้ำใสหมูเด้ง', '', '47.jpg', '45'),
(8, 'PD1008', 'ลาเมง', '', '48.jpg', '50'),
(9, 'PD1009', 'แห้งหมูสับ', '', '49.jpg', '45'),
(10, 'PD1010', 'น้ำใสเครื่องใน', '', '50.jpg', '50'),
(11, 'PD1011', 'น้ำใสลูกชิ้นปลา', '', '51.jpg', '45'),
(12, 'PD1012', 'ลาเมงซอสญี่ปุ่น', '', '52.jpg', '50');

-- --------------------------------------------------------

--
-- Table structure for table `producttest`
--

CREATE TABLE `producttest` (
  `id` int(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  `amount` int(7) NOT NULL,
  `price` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producttest`
--

INSERT INTO `producttest` (`id`, `name`, `amount`, `price`) VALUES
(1, 'เครื่องในเย็นตาโฟ', 1, 45),
(2, 'บะหมี่ต้มยำ', 1, 50),
(3, 'หมี่แห้งไม่ผัก', 1, 35),
(4, 'บะหมี่น้ำตกหมู', 1, 45);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `pid` int(11) NOT NULL,
  `pname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`pid`, `pname`) VALUES
(1, 'กรุงเทพมหานคร'),
(2, 'กระบี่ '),
(3, 'กาญจนบุรี '),
(4, 'กาฬสินธุ์ '),
(5, 'กำแพงเพชร '),
(6, 'ขอนแก่น '),
(7, 'จันทบุรี '),
(8, 'ฉะเชิงเทรา '),
(9, 'ชลบุรี '),
(10, 'ชัยนาท '),
(11, 'ชัยภูมิ '),
(12, 'ชุมพร '),
(13, 'เชียงราย '),
(14, 'เชียงใหม่ '),
(15, 'ตรัง '),
(16, 'ตราด '),
(17, 'ตาก '),
(18, 'นครนายก '),
(19, 'นครปฐม '),
(20, 'นครพนม '),
(21, 'นครราชสีมา '),
(22, 'นครศรีธรรมราช '),
(23, 'นครสวรรค์ '),
(24, 'นนทบุรี '),
(25, 'นราธิวาส '),
(26, 'น่าน '),
(27, 'บึงกาฬ '),
(28, 'บุรีรัมย์ '),
(29, 'ปทุมธานี '),
(30, 'ประจวบคีรีขันธ์ '),
(31, 'ปราจีนบุรี '),
(32, 'ปัตตานี '),
(33, 'พระนครศรีอยุธยา '),
(34, 'พังงา '),
(35, 'พัทลุง '),
(36, 'พิจิตร '),
(37, 'พิษณุโลก '),
(38, 'เพชรบุรี '),
(39, 'เพชรบูรณ์ '),
(40, 'แพร่ '),
(41, 'พะเยา '),
(42, 'ภูเก็ต '),
(43, 'มหาสารคาม '),
(44, 'มุกดาหาร '),
(45, 'แม่ฮ่องสอน '),
(46, 'ยะลา '),
(47, 'ยโสธร '),
(48, 'ร้อยเอ็ด '),
(49, 'ระนอง '),
(50, 'ระยอง '),
(51, 'ราชบุรี '),
(52, 'ลพบุรี '),
(53, 'ลำปาง '),
(54, 'ลำพูน '),
(55, 'เลย '),
(56, 'ศรีสะเกษ '),
(57, 'สกลนคร '),
(58, 'สงขลา '),
(59, 'สตูล '),
(60, 'สมุทรปราการ '),
(61, 'สมุทรสงคราม '),
(62, 'สมุทรสาคร '),
(63, 'สระแก้ว '),
(64, 'สระบุรี '),
(65, 'สิงห์บุรี '),
(66, 'สุโขทัย '),
(67, 'สุพรรณบุรี '),
(68, 'สุราษฎร์ธานี '),
(69, 'สุรินทร์ '),
(70, 'หนองคาย '),
(71, 'หนองบัวลำภู '),
(72, 'อ่างทอง '),
(73, 'อุดรธานี '),
(74, 'อุทัยธานี '),
(75, 'อุตรดิตถ์ '),
(76, 'อุบลราชธานี '),
(77, 'อำนาจเจริญ');

-- --------------------------------------------------------

--
-- Table structure for table `sex`
--

CREATE TABLE `sex` (
  `sid` int(10) NOT NULL,
  `sname` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sex`
--

INSERT INTO `sex` (`sid`, `sname`) VALUES
(1, 'ชาย'),
(2, 'หญิง');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrdersID`);

--
-- Indexes for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`DetailID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `producttest`
--
ALTER TABLE `producttest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `sex`
--
ALTER TABLE `sex`
  ADD PRIMARY KEY (`sid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `cid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrdersID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `DetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `producttest`
--
ALTER TABLE `producttest`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sex`
--
ALTER TABLE `sex`
  MODIFY `sid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
