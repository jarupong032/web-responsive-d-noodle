<?php require('connect.php'); ?>
<html>
<head>
<title>เพิ่มข้อมูลลูกค้า</title>
<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<meta charset="utf-8">
</head>
<body>


<?php

$sql = "SELECT * FROM provinces";
$result = mysqli_query($con,$sql);
$provinces = mysqli_fetch_all($result,MYSQLI_ASSOC);


if(isset($_POST['save'])) {
  $fullname = $_POST['fullname'];
  $username = $_POST['username'];
  $password = ($_POST['password']);
  $email = $_POST['email'];  
  $province = $_POST['province'];
  $sex=$_POST['sex'];
  $tel=$_POST['tel'];
  $at=$_POST['at'];
$district=$_POST['district'];
$amphoe=$_POST['amphoe'];
$province=$_POST['province'];
$zipcode=$_POST['zipcode'];
$pic=$_POST['pic'];
  $sql = "INSERT INTO customers(username,password,fullname,email,sex,tel,at,status,district,amphoe,province,zipcode,pic) VALUES('$username','$password','$fullname','$email','$sex','$tel','$at','1','$district','$amphoe','$province','$zipcode','$pic');";
  mysqli_query($con,$sql);
  header('location:customer.php');
}
else if(isset($_POST['back'])){
header('location:customer.php');
}

?>
<form action="" method="post" class="" id="demo1">
<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">

<tr height="8%" width="100%" align="top">
	<td Align="center" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:35;" colspan="2" >
	<i class="fa fa-plus-square-o" aria-hidden="true"></i> เพิ่มข้อมูลลูกค้า </td>
</tr >
<tr>
	<td align="center" valign="top" colspan="2">
	<br>	
			<table height="70%" width="70%" style="color:#fff;">
			<tr>
			<td>
			</td><img id="picPreview"style="width:200;margin:5px"><td>
<form action="" method="post" enctype="multipart/form-data">
    <label>
        <input type="file" name="pic" accept="image/*" style="width:300;">
    </label></td>
	</tr>
	
			<tr>
				<td width="150px">Full Name :</td>
				<td colspan="3"><input type="text" name="fullname" placeholder="Name" "></td>
			</tr>
			<tr>
				<td width="150px">Username :</td>
				<td colspan="3"><input type="text" name="username" placeholder="Username" ></td>
			</tr>
			<tr>
				<td width="150px">Password :</td>
				<td colspan="3"><input type="password" name="password" placeholder="Password" ></td>
			</tr>
			<tr>
				<td width="150px">Gender :</td>
				<td style="color:#FFF" colspan="3">
							<select   name="sex" style="border-color:#be8943;background:#222631;color:#fff;padding:2px 5px 2px 5px;" class="form-control">
							<option value="เพศ" >Gender
							 <?php
									$sql="select * from sex";
									$result=mysqli_query($con,$sql);
									while($row2=mysqli_fetch_assoc($result))
									{
										$selected=($row[sid] == $row[sid])?:"";
							echo "<option value='.$row2[sid].' $selected>$row2[sname]";
									}
							?>
							</select>
				</td>
			</tr>
			<tr>
				<td width="150px">Tel:</td>
				<td colspan="3"><input type="text" name="tel" placeholder="Tel" ></td>
			</tr>
			<tr>
				<td width="150px"><label>E-mail :</label></td>
				<td colspan="3"><input type="email" name="email" placeholder="Email" ></td>
			</tr>
			<tr>
				<td width="150px">Addess:</td>
				<td colspan="3"><input type="text" name="at" placeholder="Address" ></td>
			</tr>
			<tr>
				<td width="150px">District:</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="district" placeholder="District" class="uk-input uk-width-1-1" style="background:transparent;">
					  </div>
				</td>
				<td width="150px">Amphoe:</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="amphoe" placeholder="Amphoe" class="uk-input uk-width-1-1" style="background:transparent;">
					  </div>
				</td>
			</tr>			
			<tr>
				<td width="150px">Province:</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="province" placeholder="Province" class="uk-input uk-width-1-1" style="background:transparent;">
					  </div>
				</td>
				<td width="150px">Zipcode:</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="zipcode" placeholder="Zipcode" class="uk-input uk-width-1-1" style="background:transparent;">
					  </div>
				</td>
			</tr>		
	<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
$('[name="pic"]').change(function(){
 
    /* original file upload path */
    $('#picPath').text($(this).val());
 
    var file = this.files[0];
    /* get picture details */
    $('#picLastModified').text(file.lastModified);
    $('#picLastModifiedDate').text(file.lastModifiedDate);
    $('#picName').text(file.name);
    $('#picSize').text(file.size);
    $('#picType').text(file.type);
 
    /* set image preview */
    if( ! file.type.match(/image.*/))
    {
        return true;
    }
    var reader = new FileReader();
    reader.onload = function (event)
    {
        $('#picPreview').attr('src', event.target.result);
 
        /* get image dimensions */
        var image = new Image();
        image.src = reader.result;
        image.onload = function()
        {
            $('#picDimensionsWidth').text(image.width);
            $('#picDimensionsHeight').text(image.height);
        };
 
    }
    reader.readAsDataURL(file);
 
});
</script>

			</table>
	</td>
</tr>
<tr height="75px" width="100%" bgcolor="#222631">
		<td align="center">		
		<button onclick="window.location.href='customer.php'" name="back"  style="padding:0.7em;font-size:18;border-width:2;color:#fff"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> กลับสู่เมนู </button>			
		</td>
	<td  align="center">
	<button type="submit"  name="save" style="padding:0.7em;font-size:18;border-width:2;color:#fff"><i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึก</button>
	</td>
</tr>
</form>
</table>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
    
    <!-- dependencies for zip mode -->
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
    <!-- / dependencies for zip mode -->

    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    
    <script type="text/javascript" src="./jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
    
    <script type="text/javascript">
        /******************\
         *     DEMO 1     *
        \******************/ 
        // demo 1: load database from json. if your server is support gzip. we recommended to use this rather than zip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

        /******************\
         *     DEMO 2     *
        \******************/ 
        // demo 2: load database from zip. for those who doesn't have server that supported gzip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.zip', 
            $search: $('#demo2 [name="search"]'),

            onDataFill: function(data){
                console.log(data)
                var html = '<b>ที่อยู่:</b> ตำบล' + data.district + ' อำเภอ' + data.amphoe + ' จังหวัด' + data.province + ' ' + data.zipcode;
                $('#demo2-output').prepend('<div class="uk-alert-warning" uk-alert><a class="uk-alert-close" uk-close></a>' + html + '</div>');
            }

        });
    </script> 
</body>
</html>

<style>
	 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;

}
button {  
  background: transparent;
  
  
  font-size: 1.3em;
  border: solid 1px #be8943;
  padding: 0.5em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  padding: 0 0 0.4em 0em;
  width: 50%;
  margin-left: 25%;
  margin-bottom: 1em;
}
@media (max-width: 550px) {
  form {
  width: 90%;
  margin-left: 3%;
  padding-top: 5%;
}
  input {
    font-size: 1em;
  }
}
input {  
  background: transparent;
  width: 100%;
  
  font-size: 1.1em;
  border: solid 1px #be8943;
  padding: 0.3em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
input:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;
}


select{
border-color:#be8943;
background: transparent;
color:#fff;
border-style: solid;
border-width: 1px 1px 1px 1px; 

}

select:hover { 
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900; 
}
</style>


