<?php require('connect.php'); ?>
<html>
 <head>
  <meta charset="UTF-8">
  <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
 <title>D-NOODlE</title>
 </head>
 <body>
	<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">
	<tr height="10%" width="100%">
		<td Align="center" colspan="3" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:35;"><i class="fa fa-users" aria-hidden="true"></i> ข้อมูลลูกค้า</td>
	</tr >
	<tr>
		<td  align="center" valign="top" colspan="3">
			<br>		
							<?php
							  $sql = "SELECT * FROM customers LEFT JOIN provinces ON (customers.province = provinces.pid)";
							  $result = mysqli_query ($con ,$sql);
							  $customers = mysqli_fetch_all($result,MYSQLI_ASSOC);
							  $i = 1;
							 foreach ($customers as $customer) {
							echo '<li style="display:inline-block;">';
								echo '<div class="card-group" style="width:20rem;">';
									echo '<div class="card" style="background:#212530;border-radius:8px;margin:5px;border: 1px solid #000;">';										
										echo "<img class='card-img-top' src='picmem/" . $customer["pic"] ." ' height='180'>";
										echo '<div class="card-body">';
											echo '<table style="color:#fff;">';
												echo '<tr>';
													echo '<td>';
														echo 'Name: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['fullname'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Username: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['username'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Email: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['email'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Tel: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['tel'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Addess: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['at'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'District: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['district'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Amphoe: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['amphoe'] . '';
													echo '</td>';												
												echo '</tr>';
												echo '<tr>';
													echo '<td>';
														echo 'Province: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['province'] . '';
													echo '</td>';												
												echo '</tr>';						
												echo '<tr>';
													echo '<td>';
														echo 'Zipcode: ';
													echo '</td>';														
													echo '<td>';
														echo ''. $customer['zipcode'] . '<br>';
													echo '</td>';												
												echo '</tr>';
												echo '<tr align="center">';
													echo '<td colspan="2">';
														echo '<br>';
														echo '<a href="editcustomer.php?id='. $customer['cid'] .'">
														<button style="text-shadow:2px 2px 2px #000;">
														<i class="fa fa-cog" aria-hidden="true" ></i> แก้ไข</button></a>';				
														echo '&nbsp;<a href="deletecustomer.php?id='. $customer['cid'] .'">
														<button style="text-shadow:2px 2px 2px #000;padding-left:12px;padding-right:12px;">
														<i class="fa fa-times" aria-hidden="true" ></i> ลบ</button></a>';
													echo '</td>';												
												echo '</tr>';										
											echo '</table>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo'</li>';
							  }
							?>				
		</td>
	</tr>
	<tr height="10%" width="100%"bgcolor="#222631" >
		<td align="center" width="33%">		
		<button onclick="window.location.href='index2.php';"  style="padding:0.7em;font-size:18;border-width:1;color:#fff"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> กลับสู่เมนู </button>			
		</td>		
		<td align="center" width="33%">
		<button onclick="window.location.href='addcustomer.php';"  style="padding:0.7em;font-size:18;border-width:1;color:#fff"><i class="fa fa-plus-square-o" aria-hidden="true"></i> เพิ่มข้อมูล </button>	
		</td>
	</tr>
	</table>
</body>
</html>
<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#392b29;
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0; 
}
button {  
  background: transparent; 
  font-size: 1.0em;
  border: solid 1px #be8943;
  padding: 0.2em ;
  color: #bdc3c7;
  transition: all 0.6s;
  border-radius:3px;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;  
  box-shadow:0px 0px 5px #ff9900;
}
</style>