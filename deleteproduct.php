<?php require('connect.php'); ?>
<html>
<head>
<title>ลบข้อมูลสินค้า</title>
  <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  <Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
</head>
<body>

<?php
$id = $_GET['id'];

$sql = "SELECT * FROM products WHERE id=$id";
$result = mysqli_query($con,$sql);
$product = mysqli_fetch_all($result,MYSQLI_ASSOC);

if(isset($_POST['delete'])) {




  $sql = "DELETE FROM products  WHERE id=$id";
  mysqli_query($con,$sql);
  header('location:editproduct.php');

}

?>



<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">
<tr height="8%" width="100%">
	<td Align="center" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;font-size:25;"><i class="fa fa-times" aria-hidden="true" ></i> ลบข้อมูลสินค้า  </td>
</tr >
<tr>
	<td align="center" valign="top">	
	<br>
	<h3 style="color:#fff;">แน่ใจแล้วใช่ไหมที่จะลบข้อมูล <br>
	" <?php echo $product[0]['product_name'];?>"<br>
	ลบออกจากระบบ ?</h3>
	<form action="" method="post" class="">
	<button type="submit"  name="delete" style="width:175px;"><span class="glyphicon glyphicon-remove-circle"></span><i class="fa fa-times" aria-hidden="true" ></i>  ลบข้อมูล</button>
	</form>
	<button onclick="window.location.href='editproduct.php'" style="width:170px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> ยกเลิก</button>
	</td>
</tr>
</table>




</body>
</html>

<style>
	 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;

}
button {  
  background: transparent;
 
  margin: 10px 0px 0px 0px;
  font-size: 1.3em;
  border: solid 1px #be8943;
  padding: 0.5em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  padding: 0 0 0.4em 0em;
  width: 50%;
  margin-left: 25%;
  margin-bottom: 1em;
}
@media (max-width: 550px) {
  form {
  width: 90%;
  margin-left: 3%;
  padding-top: 5%;
}
  input {
    font-size: 1em;
  }
}
input {  
  background: transparent;
  width: 100%;
  
  font-size: 1.1em;
  border: solid 1px #be8943;
  padding: 0.3em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
input:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;
}


select{
border-color:#be8943;
background: transparent;
color:#fff;
border-style: solid;
border-width: 1px 1px 1px 1px; 

}

select:hover { 
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900; 
}
</style>

