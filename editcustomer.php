<?php require('connect.php'); ?>
<html>
<head>
<title>แก้ไขข้อมูลลูกค้า</title>
  <Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
  <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</head>
<body>

<?php
$id = $_GET['id'];

$provincesql = "SELECT * FROM provinces";
$provinceresult = mysqli_query($con,$provincesql);
$provinces = mysqli_fetch_all($provinceresult,MYSQLI_ASSOC);


$sql = "SELECT * FROM customers WHERE cid=$id";
$result = mysqli_query($con,$sql);
$oldcustomer = mysqli_fetch_all($result,MYSQLI_ASSOC);

if(isset($_POST['edit'])) {




  $fullname = $_POST['fullname'];
  $username = $_POST['username'];
  $tel = $_POST['tel'];
  $sex = $_POST['sex'];
  $at = $_POST['at'];
  $email = $_POST['email'];
  $district = $_POST['district'];
	$amphoe = $_POST['amphoe'];
	$province = $_POST['province'];
	$zipcode = $_POST['zipcode'];
$pic = $_POST['pic'];
  $sql = "UPDATE customers SET fullname='$fullname' , username='$username' , tel='$tel' , sex='$sex' , at='$at' , email='$email' , district='$district' , amphoe='$amphoe' , province='$province' , zipcode='$zipcode' , pic='$pic' WHERE cid=$id";
  mysqli_query($con,$sql);
  header('location:customer.php');

}

?>

<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">
<form action="" method="post" class="" id="demo1">
<tr height="8%" width="100%">
	<td Align="center" bgcolor="#222631" colspan="2" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:35;"><i class="fa fa-cog" aria-hidden="true" ></i> แก้ไขข้อมูลลูกค้า </td>
</tr >
<tr>
	<td align="center" valign="top" colspan="2" >	
			<table width="75%" style="color:#fff;">
			<tr align="center">
			<td colspan="2">
			<br>
			<?php echo "<img src='picmem/" . $oldcustomer[0]["pic"] ." ' width='220'>"; ?>
			</td>
			</tr>
			<tr>
			<td>
			</td>			
			<td>
<form action="" method="post" enctype="multipart/form-data">
    <label>
        <input type="file" name="pic" accept="image/*">
    </label>
	
	</td>
	</tr>	
			<tr>
				<td width="100px">ID :</td>
				<td><input type="text"  name="fullname" value="<?php echo $oldcustomer[0]['cid']; ?>" disabled></td>
			</tr>
			<tr>
				<td>Name :</td>
				<td><input type="text"  name="fullname" value="<?php echo $oldcustomer[0]['fullname']; ?>"></td>
			</tr>			
			<tr>
				<td>Username :</td>
				<td><input type="text"  name="username" value="<?php echo $oldcustomer[0]['username']; ?>"></td>
			</tr>
			<tr>
				<td>Password :</td>
				<td><input type="text"  name="password" value="<?php echo $oldcustomer[0]['password']; ?>" disabled></td>
			</tr>
			<tr>
				<td><label>E-mail :</label></td>
				<td><input type="text"  name="email" value="<?php echo $oldcustomer[0]['email']; ?>"></td>
			</tr>
			<tr>
				<td><label>Tel :</label></td>
				<td><input type="text"  name="tel" value="<?php echo $oldcustomer[0]['tel']; ?>"></td>
			</tr>
			
			<tr>
				<td><label>Addess :</label></td>
				<td><input type="text"  name="at" value="<?php echo $oldcustomer[0]['at']; ?>"></td>
			</tr>
			<tr>
				<td>District :</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="district" value="<?php echo $oldcustomer[0]['district']; ?>" class="uk-input uk-width-1-1">
					  </div>
				</td>
			</tr>
			<tr>
				<td>Amphoe :</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="amphoe" value="<?php echo $oldcustomer[0]['amphoe']; ?>" class="uk-input uk-width-1-1"  >
					  </div>
				</td>
			</tr>
			<tr>
				<td>Province :</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="province" value="<?php echo $oldcustomer[0]['province']; ?>" class="uk-input uk-width-1-1" >
					  </div>
				</td>
			</tr>
			<tr>
				<td>Zipcode :</td>
				<td>
				<div class="uk-width-1-2@m">
				  	  <input type="text" name="zipcode" value="<?php echo $oldcustomer[0]['zipcode']; ?>" class="uk-input uk-width-1-1" >
					  </div>
				</td>
			</tr>
			</table>

			<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
$('[name="pic"]').change(function(){
 
    /* original file upload path */
    $('#picPath').text($(this).val());
 
    var file = this.files[0];
    /* get picture details */
    $('#picLastModified').text(file.lastModified);
    $('#picLastModifiedDate').text(file.lastModifiedDate);
    $('#picName').text(file.name);
    $('#picSize').text(file.size);
    $('#picType').text(file.type);
 
    /* set image preview */
    if( ! file.type.match(/image.*/))
    {
        return true;
    }
    var reader = new FileReader();
    reader.onload = function (event)
    {
        $('#picPreview').attr('src', event.target.result);
 
        /* get image dimensions */
        var image = new Image();
        image.src = reader.result;
        image.onload = function()
        {
            $('#picDimensionsWidth').text(image.width);
            $('#picDimensionsHeight').text(image.height);
        };
 
    }
    reader.readAsDataURL(file);
 
});
</script>
<br>
	</td>
</tr>
<tr height="75px" width="100%" bgcolor="#222631">
	<td align="center">		
	<button onclick="window.location.href='customer.php';"  style="padding:0.7em;font-size:18;border-width:2;color:#fff width: 175px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i><a style="text-decoration:none;color:#fff;" href="customer.php"> กลับสู่เมนู</a></button>			
	</td>
	<td align="center">
	<button type="submit"  name="edit" style="padding:0.7em;font-size:18;border-width:2;color:#fff width: 175px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึก</button>
	</td>
</tr>
</table>
			</table>
	</td>
</tr>
</form>
</table>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
    
    <!-- dependencies for zip mode -->
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
    <!-- / dependencies for zip mode -->

    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript" src="./jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
    
    <script type="text/javascript" src="./jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
    
    <script type="text/javascript">
        /******************\
         *     DEMO 1     *
        \******************/ 
        // demo 1: load database from json. if your server is support gzip. we recommended to use this rather than zip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

        /******************\
         *     DEMO 2     *
        \******************/ 
        // demo 2: load database from zip. for those who doesn't have server that supported gzip.
        // for more info check README.md
        $.Thailand({
            database: './jquery.Thailand.js/database/db.zip', 
            $search: $('#demo2 [name="search"]'),

            onDataFill: function(data){
                console.log(data)
                var html = '<b>ที่อยู่:</b> ตำบล' + data.district + ' อำเภอ' + data.amphoe + ' จังหวัด' + data.province + ' ' + data.zipcode;
                $('#demo2-output').prepend('<div class="uk-alert-warning" uk-alert><a class="uk-alert-close" uk-close></a>' + html + '</div>');
            }

        });
    </script>

</body>
</html>

<style>
	 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;

}
button {  
  background: transparent;
  
  
  font-size: 1.3em;
  border: solid 1px #be8943;
  padding: 0.5em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  padding: 0 0 0.4em 0em;
  width: 50%;
  margin-left: 25%;
  margin-bottom: 1em;
}
@media (max-width: 550px) {
  form {
  width: 90%;
  margin-left: 3%;
  padding-top: 5%;
}
  input {
    font-size: 1em;
  }
}
input {  
  background: transparent;
  width: 100%;
  
  font-size: 1.1em;
  border: solid 1px #be8943;
  padding: 0.3em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
input:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;
}


select{
border-color:#be8943;
background: transparent;
color:#fff;
border-style: solid;
border-width: 1px 1px 1px 1px; 

}

select:hover { 
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900; 
}
</style>