<?php require('connect.php'); ?>
<html>
<head>
<title>ข้อมูลสินค้า</title>

<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
<meta charset="UTF-8">
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body>
	<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">
	<tr height="8%" width="100%">
		<td Align="center" colspan="2" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:35;"><i class="fa fa-tasks" aria-hidden="true"></i> จัดการสินค้า </td>
	</tr >
	<tr height="82%" width="100%">
		<td align="center" valign="top" colspan="2">
		<br>		
									<table  style="color:#fff" height="80%" width="90%" >
							  <tr>
								<td align="center">#</td>
								<td align="center">Name</td>
								<td align="center">price</td>
								<td align="center">MENU</td>								
							  </tr>
							<?php
								$sql = "SELECT * FROM products";
								$result = mysqli_query ($con ,$sql);
								$product = mysqli_fetch_all($result,MYSQLI_ASSOC);

							  $i = 1;
							 foreach ($product as $product) {
								echo '<tr>';
								echo '<td align="center">'.  $i.  '</td>';
						
								echo '<td align="center">'. $product['product_name'] . '</td>';
								echo '<td align="center">'. $product['price'] . '</td>';
								echo '<td align="center"><a href="editproduct2.php?id='. $product['id'] .'"><button ><i class="fa fa-cog" aria-hidden="true" ></i> แก้ไข</button></a>';
								echo '&nbsp;<a href="deleteproduct.php?id='. $product['id'] .'"><button ><i class="fa fa-times" aria-hidden="true" ></i>ลบ</button></a></td></tr>';

								
								$i++;
							  }
							?>

							</table>
		</td>
	</tr>
	<tr height="10%" width="100%"bgcolor="#222631">
		<td align="center" width="50%">		
		<button onclick="window.location.href='product.php';"  style="padding:0.7em;font-size:18;border-width:2;color:#fff;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> กลับสู่หน้าเลือก</button>			
		</td>
		<td align="center" width="50%">
		<button onclick="window.location.href='printlist.php';"  style="padding:0.7em;font-size:18;border-width:2;color:#fff; width:150px;"><i class="fa fa-print" aria-hidden="true"></i> ปริ้น </button>	
		</td>
	</tr>
	</table>
</body>
</html>

<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0; 
}
button {  
  background: transparent;
  font-size: 1.0em;
  border: solid 1px #be8943;
  padding: 0.2em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
</style>
