<?php require('connect.php'); ?>
<html>
<head>
<title>ข้อมูลลูกค้า</title>
<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<meta charset="UTF-8">
</head>
<body>
	<table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;">
	<tr height="8%" width="100%">
		<td Align="center"  bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;font-size:35;" colspan="2"><i class="fa fa-retweet" aria-hidden="true"></i> การจัดการตำแหน่ง </td>
	</tr >
	<tr height="82%" width="100%">
		<td align="center" valign="top" colspan="2">
		<br>		
									<table style="color:#fff" height="70%" width="60%">
							  <tr>
								<td>#</td>
								<td>ชื่อ-สกุล</td>
								<td align="center">ตำแหน่ง</td>								
								<td align="center">MENU</td>								
							  </tr>
							<?php
							  $sql = "SELECT * FROM customers LEFT JOIN provinces ON (customers.province = provinces.pid)";
							  $result = mysqli_query ($con ,$sql);
							  $customers = mysqli_fetch_all($result,MYSQLI_ASSOC);
							  $i = 1;
							 foreach ($customers as $customer) {
								echo '<tr>';
								echo '<td width="10%" >'.  $i.  '</td>';
								echo '<td>'. $customer['fullname'] . '</td>';
								echo '<td width="20%" align="center">'. $customer['status'] . '</td>';								
								echo '<td width="20%" align="center"><a href="editstatus2.php?id='. $customer['cid'] .'"><button><i class="fa fa-cog" aria-hidden="true" ></i> แก้ไข</button></a>';
								echo '</td>';
								echo '</tr>';
								$i++;
							  }
							?>

							</table>
		</td>
	</tr>
	<tr height="10%" width="100%"bgcolor="#222631">
		<td align="center" width="70%"> 		
		<button onclick="window.location.href='index2.php';" style="width:150px; padding:0.7em;font-size:18;border-width:2;color:#fff"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> กลับสู่เมนู </button>
		</td>
		<td align="left" width="30%" style="color:#fff;padding:0px 0px 0px 5%;">
		<font size="3px">
		ตำแหน่ง 2 = Admin <br>
		ตำแหน่ง 1 = User
		</font>
		</td>
	</tr>
	</table>
</body>
</html>

<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0; 
}
button {  
  background: transparent;
  width: 70px;  
  font-size: 1.0em;
  border: solid 1px #be8943;
  padding: 0.2em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
</style>