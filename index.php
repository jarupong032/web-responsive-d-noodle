<html lang="en">
<meta charset="UTF-8">
<head>
<title>D-NOODlE</title>
 </head>
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <link rel="stylesheet" href="css/font-awesome.min.css">
 <!-- Optional JavaScript -->
 <!-- jQuery first, then Popper.js, then Bootstrap JS -->
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
 integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
 </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" 
 integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
 </script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" 
 integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous">
 </script>

<body>
<?php include 'nav.php';?>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="pictures/Frist.jpg" width="512"  alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="pictures/2.jpg" width="512"  alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="pictures/3.jpg" width="512" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<hr>
<div class="container" align="center">
	<h1 style="color:#fff;margin-bottom:30px;margin-top:20px;">RECOMMEND MENU</h1>
	<div class="row">
			<div class="col-sm">
			  <img src="pictures/48.jpg" width="300" height="200" class="cardRmenu">
			</div>
			<div class="col-sm">
			  <img src="pictures/42.jpg" width="300"  height="200" class="cardRmenu">
			</div>
			<div class="col-sm">
			  <img src="pictures/43.jpg" width="300"  height="200" class="cardRmenu">
			</div>
	</div>
	<div class="row">
			<div class="col-sm">
			  <img src="pictures/44.jpg" width="300" height="200" class="cardRmenu">
			</div>
			<div class="col-sm">
			 <img src="pictures/45.jpg"  width="300" height="200" class="cardRmenu">
			</div>
			<div class="col-sm">
			  <img src="pictures/46.jpg" width="300" height="200" class="cardRmenu">
			</div>
	</div>
</div>

 
<hr>
<div class="container" align="center">
	<h1 style="color:#fff;margin-bottom:30px;margin-top:20px;">BEST SELLERS</h1>
	<div class="row">
		<div class="col-sm">
		<img src="pictures/45.jpg" class="cardRmenu" width="60%" alt="Responsive image">	
		</div>
	</div>
</div>

<hr>
<div class="container-fluid">	
	<div class="row">
		<div class="col" align="center">		
		<img src="pictures/swit.png" height="5%" align="right">		
		<img src="pictures/f.png" height="5%" align="right">
		<br>
		<h6 style="color:#fff;">@copylight</h6>
		</div>
	</div>
</div>
</div>
</body>
</html>

<style>
html,body
 {
 height:100%;
 margin:0px;
 }
 body{
 background-color:#392b29;
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family: Verdana,sans-serif;margin:0;
 }
 hr {
    height: 1px;
    color: #be8943;
    background: #be8943;
	}
.cardRmenu{
border-radius:5px;
border:2px solid #be8943;
margin-bottom:10px;
}
</style>
