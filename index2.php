<?php require('connect.php');
session_start();

if (!isset($_SESSION['fullname'])) {
  echo "<meta http-equiv='refresh' content='1;URL=3.php'>";
  exit;
}

 ?>
<html>
<head>
<title>Admin</title>
<meta charset="UTF-8">
<Link  rel="stylesheet" type="text/css" href="index.css">
<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body>
  <?php
$sql = "SELECT * FROM customers";
$result = mysqli_query($con,$sql);
$customers = mysqli_num_rows($result);

$sql = "SELECT * FROM provinces";
$result = mysqli_query($con,$sql);
$provinces = mysqli_num_rows($result);

   ?>

<div class="container" height="100%" width="100%" border="2" style="border-color:#be8943;" align="center">
<div class="row" height="8%" width="100%">
	<div class="col" Align="center" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:45;">WELCOME ADMIN</div>
</div >
	<!--<div class="col-sm" style="padding-top: 2%;">
	<button onclick="window.location.href='#';" >
	<i class="fa fa-user" aria-hidden="true"></i> <?php echo $_SESSION['username']; ?></button>
	</div>-->
<br>
<div class="container">
<div class="row">
	<div class="col-sm" style="padding-top: 2%;">
	<button onclick="window.location.href='customer.php';" >
	<i class="fa fa-users" aria-hidden="true"></i> ข้อมูลลูกค้า (<?php echo $customers; ?>) </button>
	</div>
	<div class="col-sm" style="padding-top: 2%;">
	<button onclick="window.location.href='editstatus.php';" ><i class="fa fa-retweet" aria-hidden="true"></i> การจัดการตำแหน่ง  </button>
	</div>
</div>
<div class="row">
	<div class="col-sm" style="padding-top: 2%;">
	<button onclick="window.location.href='product.php';" ><i class="fa fa-tasks" aria-hidden="true"></i> จัดการสินค้า  </button>
	</div>
	<div class="col-sm" style="padding-top: 2%;">
	<button onclick="window.location.href='logout.php';"  ><i class="fa fa-sign-out" aria-hidden="true" style="color:red"></i> ออกระบบ  </button>	
	</div>
</div>
</div>
</div>
</body>
</html>

<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family:Verdana,sans-serif;margin:0;

}
button {  
  background: transparent;
  width: 100%;
  
  font-size: 1.3em;
  border: solid 1px #be8943;
  padding: 0.5em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  padding: 0 0 0.4em 0em;
  width: 100%;
  
}
  input {
    font-size: 1em;
  }
}
</style>
