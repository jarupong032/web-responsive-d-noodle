<?php
require('connect.php'); 
session_start();
?>
<nav class="navbar navbar-expand-lg navbar-dark" style="background-color:#212530;box-shadow:0px 4px 0px #577b87;margin-bottom:5px;">
	<a class="navbar-brand" href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
	</button>


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="15.php"><i class="fa fa-server" aria-hidden="true"></i> Menu</a>
      </li>	 
	 <?php
	 if (isset($_SESSION['cid']))
	 { 
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="11.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart</a>';
		echo'</li>';
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="9.php"><i class="fa fa-book" aria-hidden="true"></i> Order</a>';
		echo'</li>';
		echo'</ul>';
		echo'<ul class="navbar-nav">';
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="6.php"><i class="fa fa-user" aria-hidden="true"></i> '.$_SESSION['username'].'</a>';
		echo'</li>';
		echo'<li class="nav-item">';
		echo'<div class="menu">';
        echo'<a class="btn btn-skyblue my-2 my-sm-0" href="3.php" style="border-color:#be8943;"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>';
	}else{
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="9-1.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart</a>';
		echo'</li>';
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="9-1.php"><i class="fa fa-book" aria-hidden="true"></i> Order</a>';
		echo'</li>';
		echo'</ul>';
		echo'<ul class="navbar-nav">';
		echo'<li class="nav-item">';
        echo'<a class="nav-link" href="5.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a>';
		echo'</li>';
		echo'<li class="nav-item">';
		echo'<div class="menu">';
        echo'<a class="btn btn-skyblue my-2 my-sm-0" href="3.php" style="border-color:#be8943;"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>';
	}
	?>
	  </div>	
      </li>
	  </ul>
</div>
</nav>

<style>
.btn-skyblue{
color:#fff;
	}
 .btn-skyblue:hover {     
 color:#be8943;	 
	}
.navbar-toggler{
 border:0px;
	}
</style>