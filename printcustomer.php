<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
require('connect.php');
require('fpdf.php');
mysqli_query($con,"SET NAMES TIS620");
$sql = "SELECT * FROM customers";
$result = mysqli_query($con,$sql);
$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);
class PDF extends FPDF
{

// Page header
function Header()
{
    // Logo
    $this->Image('pictures/G1.gif',75,17,50);
	    $this->Ln(20);
    // Arial bold 15
    $this->SetFont('THSarabunNew Bold','',20);
    // Move to the right
    $this->Cell(170);
    // Title
    $this->Cell(1,-45,Date("d:m:Y"),0,0,'L');
    // Line break
    $this->Ln(20);
    $this->SetFont('THSarabunNew','',20);


	$this->SetFont('THSarabunNew Bold','',20);
	$this->SetFillColor(128,128,128);
	$this->SetTextColor(255,255,255);
	$this->Cell(1);
	$this->Cell(10,12,"#",1,0,'C',TRUE);
	$this->Cell(25,12,"Photo",1,0,'C',TRUE);
	$this->Cell(50,12,"Fullname",1,0,'C',TRUE);
	$this->Cell(40,12,"Email",1,0,'C',TRUE);
	$this->Cell(30,12,"TEl",1,0,'C',TRUE);
	$this->Cell(30,12,"Username",1,1,'C',TRUE);
  

}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-12);
    // Arial italic 8
    $this->SetFont('THSarabunNew','',12);
    // Page number
    $this->Cell(0,10,'Print by Admin',0,0,'L');
	$date = date("d M Y");
	    $this->Cell(0,10,$date,0,0,'R');

}
}

/// Instanciation of inherited class
$pdf = new PDF();
$pdf->AddFont('THSarabunNew','','THSarabunNew.php');
$pdf->AddFont('THSarabunNew Bold','','THSarabunNew Bold.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('THSarabunNew','',20);
$i = 1;
$image_height = 20;
$image_width = 20;
foreach ($rows as $row) {

	$pdf->SetFont('THSarabunNew','',18);
  $pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
  $pdf->Cell(1);
	$pdf->Cell(10,30,$i,1,0,'C',TRUE);

	// get current X and Y
   $start_x = $pdf->GetX();
   $start_y = $pdf->GetY();

   // place image and move cursor to proper place. "+ 5" added for buffer
   $pdf->Image('picmem/'.$row['pic'], $pdf->GetX()+3, $pdf->GetY()+3, $image_height, $image_width) ;
   $pdf->SetXY($start_x+$image_width, $start_y);
     $pdf->Cell(-20);

	   $pdf->Cell(25,30,'',1);

	$pdf->Cell(50,30,$row['fullname'],1,0,'C',TRUE);
  
  $pdf->Cell(40,30,$row['email'],1,0,'C',TRUE);

  $pdf->Cell(30,30,$row['tel'],1,0,'C',TRUE);
$pdf->Cell(30,30,$row['username'],1,1,'C',TRUE);

$i++;
}

$pdf->Output();
?>