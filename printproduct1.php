<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
require('connect.php');
require('fpdf.php');
mysqli_query($con,"SET NAMES TIS620");

$OrdersID=$_GET['OrdersID'];
$sqlpro="select orders_detail.*,products.* from orders_detail,products WHERE orders_detail.Productcode=products.Product_code and OrdersID=$OrdersID";
$resultpro=mysqli_query($con,$sqlpro);

class PDF extends FPDF
{

// Page header
function Header()
{
    // Logo
    $this->Image('pictures/G1.gif',75,17,50);
	    $this->Ln(20);
    // Arial bold 15
    $this->SetFont('THSarabunNew Bold','',20);
$this->cell(1,-45,$_SESSION['fullname'],0,0,'L');
    // Move to the right
    $this->Cell(170);
    // Title
    $this->Cell(1,-45,Date("d:m:Y"),0,0,'L');
    // Line break
    $this->Ln(20);
    $this->SetFont('THSarabunNew','',20);


	$this->SetFont('THSarabunNew Bold','',22);
	$this->SetFillColor(139,119,101);
	$this->SetTextColor(255,255,255);
	$this->Cell(20);
	$this->Cell(10,12,"NO.",1,0,'C',TRUE);
	$this->Cell(90,12,"ProductName",1,0,'C',TRUE);
  $this->Cell(25,12,"AMOUNT",1,0,'C',TRUE);
  $this->Cell(25,12,"PRICE",1,1,'C',TRUE);

}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-12);
    // Arial italic 8
    $this->SetFont('THSarabunNew','',12);
    // Page number
  //  $this->Cell(0,10,'Thanks for shopping with us',0,0,'C');
}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AddFont('THSarabunNew','','THSarabunNew.php');
$pdf->AddFont('THSarabunNew Bold','','THSarabunNew Bold.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('THSarabunNew','',20);
$i = 1;
$total = 0;

while($rowuser=mysqli_fetch_array($resultpro)){

	$pdf->SetFont('THSarabunNew','',18);
  $pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
  $pdf->Cell(20);
	$pdf->Cell(10,12,$i,1,0,'C',TRUE);
	$pdf->Cell(90,12,$rowuser['product_name'] ,1,0,'L',TRUE);
  $pdf->Cell(25,12,$rowuser['Qty'],1,0,'C',TRUE);

  $all = $rowuser['Qty'] * $rowuser['price'];
  $total += $all; 
  $pdf->Cell(25,12,$all,1,1,'C',TRUE);
$i++;
}
 $pdf->Cell(20);
 	$pdf->Cell(125,12,'���',1,0,'C',TRUE);
	$pdf->Cell(25,12,$total,1,0,'C',TRUE);

$pdf->Output();
?>
