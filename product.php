<html>
<head>
  <title>จัดการสินค้า</title>
  <meta charset="utf-8">
	<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body>
  <table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;color:#fff;">
<tr height="8%" width="100%">
	<td Align="center" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:35;"><i class="fa fa-tasks" aria-hidden="true"></i> จัดการสินค้า </td>
</tr >
<tr>
	<td align="center" valign="top">
	<br>
	<a style="font-family:Verdana,sans-serif;margin:0;font-size:18px;">
	<br><br><br>
	<div class="container" width="100%"  >
	<div class="row">
		<div class="col-sm" style="margin-top:15px;">
		<button style="width:100%; height:100px;" onclick="window.location.href='editproduct.php';" >ลบ/แก้ไขสินค้า </button>
		</div>
		<br>
		<div class="col-sm" style="margin-top:15px;">
		<button style="width:100%; height:100px;" onclick="window.location.href='addproduct.php';" >เพิ่มข้อมูลสินค้า </button>
		</div>
	</div>
	</div>	
	</a>
	</td>
</tr>
<tr height="10%" width="100%">
	<td bgcolor="#222631" align="center">
	<button type="submit"  name="edit" onclick="window.location.href='index2.php';" style="padding:0.7em;font-size:18;border-width:2;color:#fff;">
	<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>  กลับสู่เมนู</button></td>
</tr>
</table>
</body>
</html>

<style>
 html,body{
 height:100%;
 margin:0px;}
 body{
 background-color:#222631;
 background-image: url("pictures/BG42.jpg");
 height: 100%;
 width: 100%;
 background-size: 100%;
 background-repeat: no-repeat;
 background-attachment: fixed;
 background-position: center;
 font-family:Verdana,sans-serif;margin:0;

}
button {  
  background: transparent;
  width: 250px;
  
  font-size: 1.3em;
  border: solid 1px #be8943;
  padding: 0.5em ;
  color: #bdc3c7;
  transition: all 0.6s;
}
button:hover {
  cursor:pointer;
  background: transparent;
  border-style: solid;
  border-width: 1px 1px 1px 1px;
  border-color:#587c88;
  box-shadow:0px 0px 14px #ff9900;

}
h1 {
  color: #bdc3c7;
  border-bottom: solid 1px #be8943;
  padding: 0 0 0.4em 0em;
  width: 50%;
  margin-left: 25%;
  margin-bottom: 1em;
}
@media (max-width: 550px) {
  form {
  width: 90%;
  margin-left: 3%;
  padding-top: 5%;
}
  input {
    font-size: 1em;
  }
}
</style>

