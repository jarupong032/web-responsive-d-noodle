<?php require('connect.php');
session_start();

if (!isset($_SESSION['fullname'])) {
  echo "<meta http-equiv='refresh' content='1;URL=index2.php>";
  exit;
}
?>

<html>
<head>
  <title>โปรไฟล์</title>
  <meta charset="utf-8">
  <Link  rel="stylesheet" type="text/css" href="index.css">
	<Link  rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.css">
</head>
<body>
  <table cellspacing="0" cellpadding="0" height="100%" width="100%" border="2" style="border-color:#be8943;color:#fff;">
<tr height="8%" width="100%">
	<td Align="center" bgcolor="#222631" style="color:#fff;font-family: Verdana,sans-serif;margin:0;font-size:25;"><i class="fa fa-user" aria-hidden="true"></i> Profile </td>
</tr >
<tr>
	<td align="center" valign="top">
	<br>
	<a style="font-family:Verdana,sans-serif;margin:0;font-size:18px;">
	สวัสดีครับ/ค่ะ<br>
	"คุณ <?php echo $_SESSION['fullname'];?>"<br>
	<br>
	รหัสของคุณเป็นลำดับที่<br>
	"<?php echo $_SESSION['cid'];?>"	
	</a>
	</td>
</tr>
<tr height="10%" width="100%">
	<td bgcolor="#222631" align="center"><button type="submit" class="btn btn-success btn-lg" name="edit" onclick="window.location.href='index2.php';"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>  กลับสู่เมนู</button></td>
</tr>
</table>
</body>
</html>


